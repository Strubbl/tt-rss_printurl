# TT-RSS plugin to print article URL

## About

This plugin adds the URL of the article in the button row on the left side. 

## Demo

![Demo of plugin tt-rss_printurl](tt-rss_printurl-demo.png)

## Install

- `git clone https://github.com/Strubbl/tt-rss_printurl.git /path/to/tt-rss/plugins/printurl`
- Activate plugin in admin panel
- Have fun

