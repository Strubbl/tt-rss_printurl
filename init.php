<?php
class printurl extends Plugin {
  private $host;

  function about() {
    return array(1.2,
      "prints the URL of the article in the button row on the left side",
      "strubbl",
      false);
  }

  function init($host) {
    $this->host = $host;
    $host->add_hook($host::HOOK_ARTICLE_LEFT_BUTTON, $this);
  }

  function hook_article_left_button($line) {
    $article_link = $line["link"];
    $rv = "<span>" . htmlspecialchars($article_link) . "</span>";
    return $rv;
  }

  function api_version() {
    return 2;
  }
}
?>
